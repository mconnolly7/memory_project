function  plot_DA_max
data_table = readtable('/Users/mconn24/Box Sync/papers/2019_12_31_paper_safe_optimization/paper_data/memory_data_clean.xlsx');

USE_HYPERPRIOR  = 0;
USE_SAFE_OPT    = 0;
PLOT            = 0;

BETA            = 2.9;
ETA             = 1.8;
% ETA             = 0.4;
alpha           = 1.27;


lower_bound     = 0;
upper_bound     = 4;
input_space     = (lower_bound:.05:upper_bound)';

threshold       = 0;
safe_samples    = [0 .5 1]';    
safe_set        = ones(size(input_space));
safe_set(input_space > max(safe_samples)) = 0;
safe_set        = safe_set == 1;

close all
f = figure('Position', [931 446 990 652]);

x_grid              = [.1 .4 .7];
y_grid_1            = [.65 .36 .07]+.02;

width               = .25;
height              = .27;

ax(1)               = axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
ax(2)               = axes('Position', [x_grid(2)  y_grid_1(1) width(1) height(1)]);
ax(3)               = axes('Position', [x_grid(3)  y_grid_1(1) width(1) height(1)]);

ax(4)               = axes('Position', [x_grid(1)  y_grid_1(2) width(1) height(1)]);
ax(5)               = axes('Position', [x_grid(2)  y_grid_1(2) width(1) height(1)]);
ax(6)               = axes('Position', [x_grid(3)  y_grid_1(2) width(1) height(1)]);

ax(7)               = axes('Position', [x_grid(1)  y_grid_1(3) width(1) height(1)]);
ax(8)               = axes('Position', [x_grid(2)  y_grid_1(3) width(1) height(1)]);
ax(9)               = axes('Position', [x_grid(3)  y_grid_1(3) width(1) height(1)]);


subject_list = [81 83 82];

for c1 = 1:3
%%%
    set(f, 'currentaxes', ax(c1));
    title(sprintf('Subject %d', c1))

    subject_idx = data_table.subject == subject_list(c1);
    phase_idx   = data_table.phase == 1;
    x_data      = data_table.voltage(subject_idx & phase_idx);
    y_data      = data_table.ds(subject_idx & phase_idx);

    [x_sample, x_opt_est, y_opt_est] = get_v_trajectory(x_data, y_data, ...
        BETA, ETA, alpha, safe_set, threshold, input_space, USE_HYPERPRIOR, USE_SAFE_OPT, PLOT);

    hold on
    plot(x_data, 'LineWidth',3, 'Color', .7*ones(1,3), 'LineStyle', '--')
    plot(x_opt_est, 'LineWidth',3, 'Color', 'k') 
%   	plot(3:size(x_opt_est,1), x_opt_est(3:end), 'LineWidth',3, 'Color', 'k')
    xlim([1 size(x_sample,1)])
    set(gca,'FontSize', 18)
    ylim([0 5])
    box off

    %%%%
    set(f, 'currentaxes', ax(c1+3));
    hold on
    plot(y_data, 'LineWidth',3, 'Color', .7*ones(1,3), 'LineStyle', '--')
%     plot(3:size(y_data,1), y_opt_est(3:end) ./ x_opt_est(3:end), 'LineWidth',3, 'Color', 'k')
    ds          = y_opt_est ./ x_opt_est;
    ds(1:2)     = 0;
    plot(ds, 'LineWidth',3, 'Color', 'k')
   	plot([0 100], [0 0], 'k:')
    ylim([-1 1])
    xlim([1 size(x_sample,1)])
    set(gca,'FontSize', 18)
    box off

    %%%
    set(f, 'currentaxes', ax(c1+6));
    hold on
    plot(y_data .* x_data, 'LineWidth',3, 'Color', .7*ones(1,3), 'LineStyle', '--')
    plot(y_opt_est, 'LineWidth',3, 'Color', 'k')
%     plot(3:size(y_opt_est,1),y_opt_est(3:end), 'LineWidth',3, 'Color', 'k')
    plot([0 100], [0 0], 'k:')
    xlim([1 size(x_sample,1)])
    ylim([-4 3])
    set(gca,'FontSize', 18)
    box off

end

set(f, 'currentaxes', ax(3));
l = legend({'Sample', 'Estimated Optimal'}, 'box', 'off', 'Position', [0.7730 0.8794 0.1951 0.0421]);

for c1 = 1:6
    set(f, 'currentaxes', ax(c1));
    xticklabels([]);
end

for c1 = [2 3 5 6 8 9]
    set(f, 'currentaxes', ax(c1));
    yticklabels([]);
end

set(f, 'currentaxes', ax(1));
ylabel('Voltage (V)')

set(f, 'currentaxes', ax(4));
ylabel('DS (a.u.)')


set(f, 'currentaxes', ax(7));
ylabel('DA (a.u.)')
xlabel('Sample')

end




function [x_sample, x_opt_est, y_opt_est] = get_v_trajectory(x_data, y_data, ...
    BETA, ETA, alpha, safe_set, threshold, input_space, USE_HYPERPRIOR, USE_SAFE_OPT, PLOT)

for c1 = 3:size(x_data,1)
    

    X_v             = x_data(1:c1);
    Y_ds            = y_data(1:c1);

    nan_idx         = isnan(Y_ds);

    X_v             = X_v(~nan_idx);
    Y_ds            = Y_ds(~nan_idx);

    [x_sample(c1,1), x_opt_est(c1,1), y_opt_est(c1,1), new_safe_set, safe_ucb] = safe_opt_update_memory(X_v, Y_ds, BETA, ETA, alpha, ...
        safe_set, threshold, input_space, USE_HYPERPRIOR, USE_SAFE_OPT, PLOT);

end 

end






