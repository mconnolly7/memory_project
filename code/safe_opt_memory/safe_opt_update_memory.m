function [x_sample, x_opt_est, y_opt_est, safe_set, safe_ucb] = safe_opt_update_memory(X_v, Y_ds, beta, eta, alpha, safe_set, threshold, input_space, USE_HYPERPRIOR, USE_SAFE, PLOT)
  
% Construct DS model
objective_model = construct_ds_model(X_v, Y_ds, USE_HYPERPRIOR, alpha);

% Resample DS model to get area
[ds_mean_est, ~, ~, ds_ci_est]     = objective_model.predict(input_space);

safety_mean             = ds_mean_est .* input_space;   % These two DS or DA
safety_uncertainty      = ds_ci_est .* input_space;     
% 
acquisition_mean       	= ds_mean_est .* input_space;   % This is static
acquisition_uncertainty	= ds_ci_est .* input_space;     % DS or DA 

% safety_mean             = ds_mean_est;   % These two DS or DA
% safety_uncertainty      = ds_ci_est;     

% acquisition_mean       	= ds_mean_est;   % This is static
% acquisition_uncertainty	= ds_ci_est;     % DS or DA 

da_mean_est             = ds_mean_est .* input_space;

% Calculate uncertainties
Q_low                	= safety_mean - beta*safety_uncertainty;
Q_high                  = safety_mean + beta*safety_uncertainty;

if USE_SAFE % Update the safe-set
    safe_set            = any([safe_set Q_low > threshold], 2);
else
    safe_set            = true(size(safety_mean));
end

t                       = size(X_v,1);
safe_input              = input_space(safe_set);
safe_af_mean            = acquisition_mean(safe_set); % af = acquisition function
safe_af_uncertainty     = acquisition_uncertainty(safe_set);

safe_ucb                = upper_confidence_bound(safe_af_mean, safe_af_uncertainty, eta, t);
[~, x_sample_idx]       = max(safe_ucb);                    
x_sample                = safe_input(x_sample_idx);

[y_opt_est, x_opt_idx]	= max(safe_af_mean);    
x_opt_est             	= safe_input(x_opt_idx,:);

if PLOT
    subplot(3,1,1); 
    cla
    objective_model.plot_mean
    objective_model.plot_standard_deviation
    positive_idx = objective_model.y_data >= 0;
    negative_idx = objective_model.y_data < 0;
    
    scatter(objective_model.x_data(positive_idx), objective_model.y_data(positive_idx), 100, 'g', 'filled')
    plot(objective_model.x_data(negative_idx), objective_model.y_data(negative_idx), 'xr', 'MarkerSize', 10, 'LineWidth',2)

    
    scatter(objective_model.x_data(end), objective_model.y_data(end), 100, 'k','LineWidth', 2)
%     xticklabels({})
    ylabel('Discriminant Score (DS)');
    set(gca,'FontSize', 14);
    ylim([-1 1])
    subplot(3,1,2); hold off
    plot(input_space,da_mean_est, 'k-', 'LineWidth', 3)
    hold on
    plot(input_space,da_mean_est + ds_ci_est, 'k:', 'LineWidth', 3)
    plot(input_space,da_mean_est - ds_ci_est, 'k:', 'LineWidth', 3)
    plot(input_space,Q_low, 'k--', 'LineWidth', 3)
    plot(input_space,Q_high, 'k--', 'LineWidth', 3)
    xlim([0 7])

    xticklabels({})
    ylabel('Discriminant Area (DA)');

    set(gca,'FontSize', 14);
    subplot(3,1,3)
    plot(safe_input, safe_ucb, 'k-', 'LineWidth', 3)
    xlim([0 7])
    xlabel('Stimulation Amplitude (V)')
%     ylabel('Upper Confididence Bound (a.u.)')
%     xlim([min(input_space) max(input_space)])
    drawnow
    set(gca,'FontSize', 14);
    pause(0.1)
end
end


