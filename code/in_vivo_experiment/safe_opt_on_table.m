clear

% data_path       = '/Users/mconn24/Box Sync/papers/2019_12_31_paper_safe_optimization/memory_data/ARN083_s.xlsx';
% data_path       = 'memory_project/data/ARN082_t.xlsx';
data_path       = 'memory_project/data/raw_data/safe_opt/STV002_t.xlsx';

data_table      = readtable(data_path);

for c1 = 3:size(data_table,1)
    safe_samples    = [0 .5 1]';
    lower_bound     = 0;
    upper_bound     = 7;
    input_space     = (lower_bound:.05:upper_bound)';

    safe_set        = ones(size(input_space));
    safe_set(input_space > max(safe_samples)) = 0;

    USE_HYPERPRIOR  = 1;
    USE_SAFE_OPT    = 1;
    PLOT            = 1;

    BETA            = 2.9;
    ETA             = 1.8;
    alpha           = 1.27;

    X_v             = data_table.voltage_new(1:c1);
    Y_ds            = data_table.DS(1:c1);

    nan_idx         = isnan(Y_ds);

    X_v             = X_v(~nan_idx);
    Y_ds            = Y_ds(~nan_idx);

    [v_new(c1), v_opt(c1), area_opt, S, ucb] = safe_opt_update_memory(X_v, Y_ds, BETA, ETA, alpha, ...
        safe_set, 0, input_space, USE_HYPERPRIOR, USE_SAFE_OPT, PLOT);

end 