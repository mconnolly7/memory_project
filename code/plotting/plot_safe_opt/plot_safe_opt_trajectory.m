clear; close all;
data_table = readtable('memory_project/data/raw_data/safe_opt/STV001_t.xlsx');

v_new = data_table.voltage_new;
v_opt = data_table.voltage_optimal_est;
ds      = data_table.DS
da      = ds .* v_new;

subplot(3,1,1)
plot(ds,'LineWidth',3, 'color', dark2(1))
set(gca,'FontSize',16)
ylabel('DS')
xlim([1 size(ds,1)])
title('STV001')

subplot(3,1,2)
plot(da, 'LineWidth',3, 'color', dark2(2))
set(gca,'FontSize',16)
ylabel('DA')
xlim([1 size(ds,1)])

subplot(3,1,3)
hold on
plot(v_new, 'ko--','LineWidth',3)
plot(v_opt, 'ko-','LineWidth',3)
set(gca,'FontSize',16)
xlabel('Samples Collected')
ylabel('Voltage')
xlim([1 size(ds,1)])
% ylim([0 4])





